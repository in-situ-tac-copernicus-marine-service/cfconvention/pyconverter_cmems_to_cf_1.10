# -*- coding: utf-8 -*-
"""
Created on Tue Jul 11 16:46:59 2023

@author: gmureau
"""
# catch all the attributes on the Fernando's google sheet and compare to a current ncdf file (old format, each attribute is mandatory)

import pandas as pd
import xarray as xr
import json

conf_path = r'C:\Users\gmureau\Desktop\Travail\mise_a_jour_cf_convention\files\work_in_progress\conf'
global_attributes_path = conf_path + '\\global_attributes.xlsx'
random_copernicus_file_path = conf_path + '\\GL_TS_DB_48522.nc'


global_attributes_to_remove = pd.read_excel(global_attributes_path, sheet_name='global_attributes_to_remove')
valid_global_attributes = pd.read_excel(global_attributes_path, sheet_name='valid_global_attributes')


global_attributes_to_remove_dict = {}
for index, row in global_attributes_to_remove.iterrows():
    global_attributes_to_remove_dict[row['Attribute']] = {'Mandatory(M)/Optionnal(O)' : row['M - Mandatory\nO - Optional'],
                                                          'Replacement Attribute': row['Replacement Attribute']
                                                          }
with open(conf_path + '\\global_attributes_to_remove.json', 'w') as output_file:
    json.dump(global_attributes_to_remove_dict, output_file, indent=4)
        

# In the previous copernicus release, each global attribute was mandatory. That means each new attribute can be found just by comparing Fernando's document and a random copernicus nc file
random_copernicus_file = xr.open_dataset(random_copernicus_file_path)
new_attributes = []
valid_global_attributes_dict = {}
for index, row in valid_global_attributes.iterrows():
    new_attributes.append(row['Attribute'] not in random_copernicus_file.attrs)
    valid_global_attributes_dict[row['Attribute']] = {'Mandatory(M)/Optionnal(O)' : row['M - Mandatory\nO - Optional'],
                                                          'New attribute': row['Attribute'] not in random_copernicus_file.attrs,
                                                          'Value' : ' '
                                                          }
    
valid_global_attributes_dict['format_version']['Value'] = '2.0'
valid_global_attributes_dict['Conventions']['Value'] = 'CF-1.11 Copernicus-InSituTAC-FormatManual-2.0.0 Copernicus-InSituTAC-ParametersList-3.3.0 Copernicus-InSituTAC-AttributesList-1.0.0'
valid_global_attributes_dict['citation']['Value'] = 'These data were collated within the Copernicus Marine Service (In Situ) and EMODnet collaboration framework. Data is made freely available by the Copernicus Marine Service and the programs that contribute to it.'
valid_global_attributes_dict['license']['Value'] = 'https://marine.copernicus.eu/user-corner/service-commitments-and-licence'
valid_global_attributes_dict['publisher_name']['Value'] = 'Copernicus Marine Service'
valid_global_attributes_dict['publisher_url']['Value'] = 'https://marine.copernicus.eu/ http://www.marineinsitu.eu/'
valid_global_attributes_dict['geospatial_lat_units']['Value'] = 'degree_north'
valid_global_attributes_dict['geospatial_lon_units']['Value'] = 'degree_east'
valid_global_attributes_dict['geospatial_vertical_units']['Value'] = 'EPSG:4979'

with open(conf_path + '\\valid_global_attributes.json', 'w') as output_file:
    json.dump(valid_global_attributes_dict, output_file, indent=4)
          
valid_global_attributes['new_attributes'] = new_attributes






