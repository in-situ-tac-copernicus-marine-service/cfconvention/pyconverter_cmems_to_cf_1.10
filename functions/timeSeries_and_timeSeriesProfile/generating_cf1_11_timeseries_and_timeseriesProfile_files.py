# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 13:52:25 2023

@author: gmureau
"""
import os
# Get the current working directory
current_dir = os.path.dirname(os.path.abspath(__file__))
import logging
logger = logging.getLogger(__name__)

def timeseries_and_timeseriesprofiles_conversion(input_file, output_file,data_type):
    #%% Dependancies
    import xarray as xr
    import numpy as np    
    import sys
    
    # Get the parent directory path
    parent_dir = os.path.dirname(os.path.abspath(current_dir))
    # Append the parent directory to sys.path
    sys.path.append(parent_dir)
    # Import the module from the parent directory
    import core_functions
    
    #%% 1) Opening nc file
    
    wd = os.path.dirname(input_file)
    buoy_file = os.path.basename(input_file)

    # file with one depth and one constant nominal position -> mixed h4/h5
    # buoy_file = "GL_TS_MO_6200086.nc" # mooring
    # buoy_file = "GL_TS_TG_ThioTG_01minute.nc" # tide gauge
    # buoy_file = "GL_TS_RF_EXSC0022.nc" # river flow
    # buoy_file = "GL_TS_MO_6100284.nc" # mesurho buoy, 48 levels max (timeseriesprofile) at the same time for the same parameter, fixed
    # buoy_file = "MO_TS_MO_HERAKLION.nc"  # buoy with TEMP_DM variable
    
    # # file with several nominal positions and two depths -> mixed h4/h5 + DEPLOYMENT new variable
    # # following this topic https://github.com/cf-convention/cf-conventions/issues/428
    # # buoy_file = "GL_TS_MO_6202404.nc" # DEPTH = 2, distinct positions: 2
    # buoy_file = "GL_TS_MO_45001.nc" # DEPTH = 4, distinct positions: 3
    # # buoy_file = "GL_TS_MO_42080.nc" # DEPTH = 1, distinct positions: 2
    
    # file with GPS positions and one depth -> h5
    # buoy_file = "GL_TS_MO_3100053.nc" # DEPTH = 1
    # buoy_file = "GL_TS_MO_14040.nc" # several positions and 20 levels max (timeseriesprofile) at the same time for the same parameter
    
    input_path = os.path.join(wd, buoy_file)
    
    
    # decode_cf = False prevents xarray of interprating the variables and change their DataTypes
    nc_file = xr.open_dataset(input_path, engine='netcdf4', decode_cf=False)
    
    
    # fileToCompress = "not_compressed"
    fileToCompress = "compressed"
    
    format = "NETCDF4_CLASSIC" # can be "NETCDF4_CLASSIC" or "NETCDF4"
    
    # for new nc file output path
    particle = 'timeserie_' + fileToCompress + '_' + format
    
    if output_file=='none':
        output_folder  = wd
        output_name = buoy_file[:-3] + '_converted_' + particle + buoy_file[-3:]
    else:
        output_folder = os.path.dirname(output_file)
        output_name = os.path.basename(output_file)
        
    output_path = os.path.join(output_folder,output_name)
    
    #%% 2)
    
    ## https://cfconventions.org/Data/cf-conventions/cf-conventions-1.10/cf-conventions.html#trajectory-data
    new_nc_file = nc_file.copy()
    
    # remove VERTICAL_SAMPLING_SCHEME
    if 'VERTICAL_SAMPLING_SCHEME' in new_nc_file.variables:
        new_nc_file.drop_vars(['VERTICAL_SAMPLING_SCHEME'])
    
    # DEPTH/HEIGHT dimension + DEPH/PRES variables management + geospatial_vertical_positive attribute
    new_nc_file = core_functions.depths_simplification(new_nc_file)
    
    # Modifying global attributes
    new_nc_file = core_functions.modify_global_attributes(new_nc_file)
    
    # sets the coordinates in the nc file
    coordinates = np.array(['TIME','LATITUDE','LONGITUDE', 'DEPH'])    
    new_nc_file, new_coords, to_add_starting_array = core_functions.timeseries_position_coordinates_management(new_nc_file)
    coordinates = np.concatenate((coordinates,new_coords))
    
    #  Determining featuretype + creating coordinate variable with the cf_role
    new_nc_file, cf_role_coordinate_name = core_functions.changing_feature_type(new_nc_file, data_type)
       
    
    # Modifying variables attributes
    new_nc_file, coordinates = core_functions.modify_variables_attributes(new_nc_file)
        
    # Updating the lists of coordinates and first variables appearing in the nc file
    starting_array = np.array([])
    for coordinate in coordinates :
        starting_array = np.concatenate( (starting_array,[coordinate]) )
        if 'ancillary_variables' in new_nc_file[coordinate].attrs:
            # variables can have several ancillary variables. They are separated by a space char ' '
            starting_array = np.concatenate( (starting_array, new_nc_file[coordinate].attrs['ancillary_variables'].split(' ')) )
    # keep the latest ocurrence
    starting_array = np.array(starting_array)
    for var in starting_array:
        where_var = np.where(starting_array == var)[0]
        if len(where_var) > 1 :
            starting_array = np.delete(starting_array,where_var[0])   
    starting_array = np.concatenate((starting_array,to_add_starting_array))
    
    
    # Sets the list of the coordinates that will automatically be set in the variables attributes (does not always work)
    new_nc_file = new_nc_file.set_coords(coordinates)
    
    # re-order the variables in the final file
    all_vars_array = np.array(list(new_nc_file.variables))
    all_vars_array = np.concatenate((starting_array,all_vars_array[~np.isin(all_vars_array,starting_array)]))
    new_nc_file = new_nc_file[all_vars_array]
    
    # variables that do not need "coordinates" attribute
    no_coordinate_attribute_variables_names = ['TIME_QC']
    if 'POSITION_QC' in new_nc_file:
        no_coordinate_attribute_variables_names.append('POSITION_QC')
    for var in new_nc_file.variables:
        if 'axis' in new_nc_file[var].attrs:
            if new_nc_file[var].attrs['axis'] == 'Z':
                no_coordinate_attribute_variables_names.append(var+'_QC')
                
    # Cannot have a string byte _FillValue
    # Replacing np.bytes_ with strings (temporary solution)
    # These variables generally do not have anything to do with the coordinates --> remove this attribute
    variables_with_byte_FillValues = []
    for var in nc_file.variables:
        if '_FillValue' in nc_file[var].attrs:
            if type(nc_file[var].attrs["_FillValue"]) == np.bytes_:
                variables_with_byte_FillValues.append(var)      
    for var in variables_with_byte_FillValues:
        new_nc_file =  new_nc_file.drop_vars(var)
    for var in no_coordinate_attribute_variables_names:
        new_nc_file[var].encoding['coordinates'] = None 
                 
    
    # Define zlib & Complevel for compressing data
    zlib = None if not fileToCompress else True
    complevel = 0 if not fileToCompress else 9
    
    # Compression settings for all variables including coordinates
    encoding = {var: {'zlib': zlib, 'complevel': complevel} for var in new_nc_file.variables}
    for variable in [var for var in starting_array if var not in ("TIME_QC", cf_role_coordinate_name)]:
        # _DM variables can be inside of the starting_array but temporarilly removed because of the byte FillValue
        if variable in new_nc_file.variables:
            encoding[variable]['_FillValue'] = None

    
    
    # deleting add_offset and scale_factor attributes + converting values (including _FillValue) to 'float32' dtype
    new_nc_file = core_functions.remove_add_offset_and_scale_factor(new_nc_file)     
    
    # export following the encoding rules, in NCDF4 classic format
    temp_path = os.path.join(output_folder,'temp.nc')
    new_nc_file.to_netcdf(temp_path,
                          format = format,
                            encoding = encoding
                          )
    
    # netCDF4 functions
    # rename the cf_role variable main dimension from 'STRING*' to 'STRLEN'
    core_functions.cf_role_variable_management(temp_path, format, cf_role_coordinate_name)
    
    # add the b' ' _FillValue attribute to DC_REFERENCE (and others than can exist)
    core_functions.variables_with_bytestr_fill_values_managing(temp_path, format, variables_with_byte_FillValues, nc_file)
    
    # compression can be long whether data and parameters are numerous
    core_functions.compression_management(temp_path, output_path, 9, format)
    
    return output_path