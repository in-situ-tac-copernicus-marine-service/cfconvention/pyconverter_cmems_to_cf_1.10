3 types of files that we are working on for the timeseries :

1: several nominal positions + GPS : SNPGPS (https://gitlab.ifremer.fr/oo-insitu/cf-convention/-/blob/main/cf-convention.adoc#user-content-nominal-and-gps-positions-are-known) --> not developped because not found yet


# The script detects the amount of distinct valid positions. If they exceed 1% of the total amount of positions, then the buoy is considered as gps.
# Else, if the buoy has several distinct positions, deployment variables will be created
# In all cases, LATITUDE and LONGITUDE coordinate variables are nominal
# Coordinates are updated, including the optionnal new coordinates (deployment or precise)
# For the moment, gps buoys have no deployment variables (clusters still have to be identified; a function is coming soon)
# Global attributes and variables attributes are updated