# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 17:25:30 2023

@author: gmureau
"""

# Function that converts cf 1.6 file to cf 1.10 file (1.8 to satisfy cf-checker)
## For nc files that present a trajectory (Bottles, Drifters, Thermosalinographs) 
## or a trajectoryProfile (Bottles, CTD, Gliders, Miniloggers, Profilers, Sea Mammals, Transmitter Chain, XBT)
## This script brings following changes:
### remove VERTICAL_SAMPLING_SCHEME
### dtype conversion from int32 (+ add_offset and scale_factor attributes) to float32 (without add_offset and scale_factor)
### All the data variables (TEMP, VGHS etc.) have a "coordinate" attribute which gives the names of the coordinates variables
### featureType global attribute is added (trajectory or trajectoryProfile)
### cf_role variable is added (TRAJECTORY or TRAJECTORY_NAME)
### Conventions global attribute is updated: 1.6 to 1.8 or 1.10
### LATITUDE, LONGITUDE, POSITION_QC now only depend on the TIME dimension
### if DEPTH==1, then all the data variables (TEMP,VGHS etc.) get this dimension removed


import os
# Get the current working directory
current_dir = os.path.dirname(os.path.abspath(__file__))
import logging
logger = logging.getLogger(__name__)

def trajectory_and_trajectoryprofiles_conversion(input_file,output_file,data_type):
    #%% Dependancies
    import xarray as xr
    import numpy as np
    import sys
    
    # Get the parent directory path
    parent_dir = os.path.dirname(os.path.abspath(current_dir))
    # Append the parent directory to sys.path
    sys.path.append(parent_dir)
    # Import the module from the parent directory
    import core_functions
    
    #%% 1) Opening nc file
    
    wd = os.path.dirname(input_file)
    traj_file = os.path.basename(input_file)
    
    # trajectory files
    # traj_file = "GL_TS_DB_21528.nc" # 4 depths # Drifter Buoy
    # traj_file = "GL_TS_DB_41670.nc" # 21 depths # Drifter Buoy
    # traj_file = "GL_TS_DB_48522.nc" # Drifter Buoy
    # traj_file = "AR_TS_TS_58GS.nc" # ThermoSalinograph
    # traj_file = "GL_TS_BO_OXYH2.nc" #  BOttle with varying positions (=trajectory) # DEPTH=1
    # traj_file = "MO_PR_CT_SicilyChannel_2004.nc" # CTD, uses "PRES" instead of "DEPH"
    # traj_file = "AR_PR_GL_6801622.nc" # GLIDER, uses "PRES" instead of "DEPH"
    # traj_file = "GL_PR_ML_EXRE0053.nc" #MINILOGER
    # traj_file = "GL_PR_PF_3901661.nc" # PROFILER, uses "PRES" instead of "DEPH"
    # traj_file = "GL_PR_SM_9901156.nc" # Sea Mammals, uses "PRES" instead of "DEPH"
    # traj_file = "GL_PR_TX_12503.nc" # Transmister chain (TX)
    # traj_file = "MO_PR_XB_LA-SUPERBA_2013.nc" # XBT
    # traj_file = "GL_TS_TS_FNFP.nc" # TS (thermosalinograph), trajectory
    # traj_file = "BO_TS_FB_SveaFB_20230911.nc"
    # traj_file = "GL_WS_DB_4102525.nc"
    
    input_path = os.path.join(wd, traj_file)
    
    
    # decode_cf = False prevents xarray of interprating the variables and change their DataTypes
    nc_file = xr.open_dataset(input_path, engine='netcdf4', decode_cf=False)
    
    
    # fileToCompress = "not_compressed"
    fileToCompress = "compressed"
    
    
    format = "NETCDF4_CLASSIC"
    # format = "NETCDF4"
    
    # for new nc file output path
    particle = 'traj_' + fileToCompress + '_' + format
    
        
    if output_file=='none':
        output_folder  = wd
        output_name = traj_file[:-3] + '_converted_' + particle + traj_file[-3:]
    else:
        output_folder = os.path.dirname(output_file)
        output_name = os.path.basename(output_file)
        
    output_path = os.path.join(output_folder,output_name)
    #%% Example H.12
    ## https://cfconventions.org/Data/cf-conventions/cf-conventions-1.10/cf-conventions.html#trajectory-data
    new_nc_file = nc_file.copy()
    
    # remove VERTICAL_SAMPLING_SCHEME
    if 'VERTICAL_SAMPLING_SCHEME' in new_nc_file.variables:
        new_nc_file.drop_vars(['VERTICAL_SAMPLING_SCHEME'])
    
        
    #  Determining featuretype + creating coordinate variable with the cf_role
    new_nc_file, cf_role_coordinate_name = core_functions.changing_feature_type(new_nc_file, data_type)
    
    # DEPTH/HEIGHT dimension + DEPH/PRES variables management + geospatial_vertical_positive attribute
    new_nc_file = core_functions.depths_simplification(new_nc_file)
    
    # Modifying global attributes
    new_nc_file = core_functions.modify_global_attributes(new_nc_file)
    
    # Modifying variables attributes
    new_nc_file, coordinates = core_functions.modify_variables_attributes(new_nc_file)
    
    # Changing LONGITUDE, LATITUDE, POSITION_QC dimensions into TIME
    new_nc_file = core_functions.dimensions_simplification(new_nc_file)
    
    # Updating the lists of coordinates and first variables appearing in the nc file
    starting_array = np.array([])
    for coordinate in coordinates :
        starting_array = np.concatenate( (starting_array,[coordinate]) )
        if 'ancillary_variables' in new_nc_file[coordinate].attrs:
            # variables can have several ancillary variables. They are separated by a space char ' '
            starting_array = np.concatenate( (starting_array, new_nc_file[coordinate].attrs['ancillary_variables'].split(' ')) )
    # keep the latest ocurrence
    starting_array = np.array(starting_array)
    for var in starting_array:
        where_var = np.where(starting_array == var)[0]
        if len(where_var) > 1 :
            starting_array = np.delete(starting_array,where_var[0])
            
                
    # re-order the variables in the final file
    all_vars_array = np.array(list(new_nc_file.variables))
    all_vars_array = np.concatenate((starting_array,all_vars_array[~np.isin(all_vars_array,starting_array)]))
    new_nc_file = new_nc_file[all_vars_array]
    
    # variables that do not need "coordinates" attribute
    no_coordinate_attribute_variables_names = ['TIME_QC','POSITION_QC']
    for var in new_nc_file.variables:
        if 'axis' in new_nc_file[var].attrs:
            if new_nc_file[var].attrs['axis'] == 'Z':
                no_coordinate_attribute_variables_names.append(var+'_QC')
     
    # Cannot have a string byte _FillValue
    # Replacing np.bytes_ with strings (temporary solution)
    # These variables generally do not have anything to do with the coordinates --> remove this attribute
    variables_with_byte_FillValues = []
    for var in nc_file.variables:
        if '_FillValue' in nc_file[var].attrs:
            if type(nc_file[var].attrs["_FillValue"]) == np.bytes_:
                variables_with_byte_FillValues.append(var)      
    for var in variables_with_byte_FillValues:
        new_nc_file =  new_nc_file.drop_vars(var)
    for var in no_coordinate_attribute_variables_names:
        new_nc_file[var].encoding['coordinates'] = None 
        
    
    # Define zlib & Complevel for compressing data
    zlib = None if not fileToCompress else True
    complevel = 0 if not fileToCompress else 9
    
    # Compression settings for all variables including coordinates
    encoding = {var: {'zlib': zlib, 'complevel': complevel} for var in new_nc_file.variables}
    for variable in [var for var in starting_array if var not in ("TIME_QC", cf_role_coordinate_name)]:
        # _DM variables can be inside of the starting_array but temporarilly removed because of the byte FillValue
        if variable in new_nc_file.variables:
            encoding[variable]['_FillValue'] = None

    
    
    # deleting add_offset and scale_factor attributes + converting values (including _FillValue) to 'float32' dtype
    new_nc_file = core_functions.remove_add_offset_and_scale_factor(new_nc_file)     
    
    # export following the encoding rules, in NCDF4 classic format
    temp_path = os.path.join(output_folder,'temp.nc')
    new_nc_file.to_netcdf(temp_path,
                          format = format,
                           encoding = encoding
                          )
    
    # netCDF4 functions
    # rename the cf_role variable main dimension from 'STRING*' to 'STRLEN'
    core_functions.cf_role_variable_management(temp_path, format, cf_role_coordinate_name)
    
    # add the b' ' _FillValue attribute to DC_REFERENCE (and others than can exist)
    core_functions.variables_with_bytestr_fill_values_managing(temp_path, format, variables_with_byte_FillValues, nc_file)
    
    core_functions.compression_management(temp_path, output_path, 9, format)
    
    return output_path

