The changes are described here : 
https://docs.google.com/document/d/1Edm3f-C19Zp5uJww8YAR3zWtaX5nxwsppJ2yIIQMfpA/edit?usp=sharing
https://docs.google.com/spreadsheets/d/1iMS3g1V9lvtMu2qSUGwRL2GaXIQ5WnZFJzaU2BxGKBE/edit?usp=sharing
https://docs.google.com/spreadsheets/d/1GI9iu1Wp3hidboXdRpIXDbFPTRSS8-YIhfZCRh90MGE/edit?usp=sharing


The python script both handles trajectory and trajectoryProfile files conversion.


# generating_cf1.11_trajectory_and_trajectoryProfile_files.py converts cf 1.6 file to cf 1.11 file  (cf-checker can only be satisfied with 1.8 version mentionned in global attribute)

## For nc files that present a trajectory (Bottles, Drifters, Thermosalinographs) or a trajectoryProfile (Bottles, Drifters, CTD, Gliders, Miniloggers, Profilers, Sea Mammals, Transmitter Chain, XBT)

## configuration files are :
### ../conf/valid_global_attributes.json file
### ../conf/variable_attributes.json file

## This script brings the following changes:
### remove VERTICAL_SAMPLING_SCHEME
### DEPH and DEPTH simplifications :
#### if DEPTH==1, then all the data variables (TEMP,VGHS etc.) get this dimension removed, and each variable that depended on DEPTH=1 is rewritten
### determining featureType + adding coordinate variable that handle the cf_role
### global attributes changes:
#### adding new global attributes and deleting older. Old attributes values can be transfered to new attributes (see ../conf/global_attributes_to_remove.json)
### variable attributes changes
### dtype conversion from int32 (+ add_offset and scale_factor attributes) to float32 (without add_offset and scale_factor)