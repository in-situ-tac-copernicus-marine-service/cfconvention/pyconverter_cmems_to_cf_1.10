# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 13:50:31 2023

@author: gmureau
"""

# Based on https://docs.google.com/document/d/1Edm3f-C19Zp5uJww8YAR3zWtaX5nxwsppJ2yIIQMfpA/edit?usp=sharing
# and https://docs.google.com/spreadsheets/d/1iMS3g1V9lvtMu2qSUGwRL2GaXIQ5WnZFJzaU2BxGKBE/edit?usp=sharing

# nc_dataset must be a netcdf dataset opened with xarray

# List of global attributes that are added:
    # geospatial_vertical_positive (depths_simplification)
    # featureType (changing_feature_type)
    # institution_country (modify_global_attributes)
# List of global attributes that are ignored (They can be filled in using a dictionnary in the modify_global_attributes function):
    # call_sign
    # comment
    # imo_platform_code
    # institution_abreviated
    # keywords
    # keywords_vocabulary
    # project

import numpy as np
import xarray as xr
import netCDF4
import requests
import xml.etree.ElementTree as ET
import json
import os
import logging
from pathlib import Path
logger = logging.getLogger(__name__)
current_path = Path(__file__)



# Remove the DEPTH dimension if DEPTH==1 and remove DEPTH dimension of each variable
# Can eventually set DEPH into scalar variable (+ remove DEPH_QC in that case)
# Add geospatial_vertical_positive global attribute
def depths_simplification(nc_dataset):
    nc_dataset_copy = nc_dataset.copy()

    
    depth_dimensions = np.array(['DEPTH','HEIGHT'])[[var in nc_dataset.dims for var in ['DEPTH','HEIGHT']]]
    deph_variables = np.array(['DEPH','PRES','HEIGHT'])[[var in nc_dataset.variables for var in ['DEPH','PRES','HEIGHT']]]

    if len(depth_dimensions) != 0 :
        depth_dimension = depth_dimensions[0]
        deph_variable = deph_variables[0]
        # Add geospatial_vertical_positive global attribute
        if depth_dimension == 'HEIGHT' :
            nc_dataset_copy.attrs['geospatial_vertical_positive'] = 'up'
        else:
            nc_dataset_copy.attrs['geospatial_vertical_positive'] = 'down'

        # DEPTH is mandatory
        """
        if nc_dataset_copy.dims[depth_dimension] == 1:
            # REMOVE DEPTH=1 AS DIMENSION
            for var in nc_dataset.variables:
                # needs to rewrite each variable that depended on the DEPTH dimension
                # for each variable, remove DEPTH dimension and keep the rest
                if depth_dimension in nc_dataset_copy.variables[var].dims:
                    dims = list(nc_dataset.variables[var].dims)
                    dims.remove(depth_dimension)
                    # delete the variable
                    nc_dataset_copy = nc_dataset_copy.drop(var)
                    # re-write the variable
                    nc_dataset_copy = nc_dataset_copy.assign(**{var : xr.Variable(dims, nc_dataset[var].values[:,0])})
                    # and its attributes
                    nc_dataset_copy[var].attrs = nc_dataset[var].attrs
            
            logger.info(depth_dimension + '=1 --> dimension removed')
        # Since all variables do not depend on "DEPTH" dimension anymore, then the dimension is automatically removed itself
        """  
        
        # Turn DEPH/PRES variable into scalar if only 1 value
        unique_deph = np.unique(nc_dataset[deph_variable])
        ## Re-writing the DEPH/PRES variable
        if len(unique_deph)==1:
            nc_dataset_copy[deph_variable] = unique_deph[0]
            nc_dataset_copy[deph_variable].attrs = nc_dataset[deph_variable].attrs
            logger.info('Only one valid value for ' + deph_variable + ' : turned into scalar variable.')
    else:
        logger.warning('No depth dimension inside of {nc_dataset.attrs["id"]}.nc file')
    
    return nc_dataset_copy


# Setting LATITUDE, LONGITUDE, POSITION_QC dimension : TIME + Remove useless dimensions
def dimensions_simplification(nc_dataset):
    # change dimension for LONGITUDE, LATITUDE, POSITION_QC
    nc_dataset.variables['LONGITUDE'].dims = "TIME"
    nc_dataset.variables['LATITUDE'].dims = "TIME"
    nc_dataset.variables['POSITION_QC'].dims = "TIME"
    nc_dataset = nc_dataset.drop_dims(['LATITUDE','LONGITUDE','POSITION'])
    return nc_dataset


# Setting the featuretype
# Also creates the coordinate variable that handles the cf_role
# Result largely depends on the depths repartition
# If there are 4 or more different depths at the same time, for the same parameter, then the featuretype become "timeseriesProfile" or "trajectoryProfile"
def changing_feature_type(nc_dataset, current_feature_type, return_max_simultaneous_measures_variable=False):    
    nc_dataset_copy = nc_dataset.copy()
    # Data variables
    data_vars = nc_dataset_copy.data_vars
    
    # We will keep only the variable with the biggest number of levels for each file
    # Maximum amount of simultaneous (for a given time) measures for each parameter (data variable)
    max_nb_of_simultaneous_dephs_for_vars = []
    for var in data_vars :
        if nc_dataset_copy[var].dtype in ['float64', 'float32', 'int32', 'int64'] and var not in ['DEPH','PRES'] and 'QC' not in var:
            if '_FillValue' in nc_dataset_copy[var].attrs:
                old_fill_value = nc_dataset_copy[var].attrs['_FillValue']
            else:
                old_fill_value = np.nan
            new_fill_value = -9999
            # Converting to int16 : prevents from using too much memory. The measure exact value is not important. What we only are looking for is presence of absence of measure
            if np.isnan(old_fill_value):
                nc_dataset_copy[var] = nc_dataset_copy[var].fillna(new_fill_value)   
            else:
                nc_dataset_copy[var] = nc_dataset_copy[var].where(nc_dataset_copy[var] != old_fill_value, new_fill_value)
            nc_dataset_copy[var] = nc_dataset_copy[var].astype('int16')  
            
            # Finding all indexes where measures do exist
            not_fill_value = ~(nc_dataset_copy[var].values == new_fill_value)
            # Getting time indexes where measures do exist
            where_not_fv = np.where(not_fill_value)
            
            if len(where_not_fv[0]) > 0:
                # We will count the maximum amount of depths where there is data, for each time index
                unique, counts = np.unique(where_not_fv[0], return_counts=True)
                max_nb_of_simultaneous_dephs_for_vars.append(np.nanmax(counts))
            else:
                logger.warning(f'{var} is completely empty inside of the {nc_dataset.attrs["id"]} file')
                max_nb_of_simultaneous_dephs_for_vars.append(np.nan)
        else:
            max_nb_of_simultaneous_dephs_for_vars.append(np.nan)
    
    # Find the data variable (parameter) index for which the number of simultaneous (same given time) values is maximum
    max_simultaneous_measures = np.nanmax(max_nb_of_simultaneous_dephs_for_vars)
    
    # Creating the coordinate variable
    if 'timeseries' in current_feature_type.lower() : #timeSeries, timeSeriesProfile
        current_feature_type = 'timeSeries'
        cf_role_coordinate_name = 'STATION'
    else: # trajectory, trajectoryProfile, Profile
        if 'profile' in current_feature_type.lower():
            if current_feature_type.lower() != 'profile':
                current_feature_type = current_feature_type.lower()
                current_feature_type = current_feature_type[:current_feature_type.index('profile')]
            else:
                current_feature_type = current_feature_type.lower()
        cf_role_coordinate_name = current_feature_type.upper()
        
    # Creating the cf_role coordinate variable. No need to know if it contains "Profile" or not
    nc_dataset = nc_dataset.assign(**{cf_role_coordinate_name: xr.DataArray(nc_dataset.attrs['platform_code'], dims=())})
    nc_dataset[cf_role_coordinate_name].attrs['long_name'] = cf_role_coordinate_name.lower()
    nc_dataset[cf_role_coordinate_name].attrs['cf_role'] = current_feature_type.lower() + '_id'
        
    # Setting the featureType
        # profile
    if 'profile' not in current_feature_type:
        if max_simultaneous_measures > 4:
            current_feature_type = current_feature_type + 'Profile'
    elif current_feature_type=='profile': # profile files are converted to trajectoryProfile
        current_feature_type = 'trajectoryProfile'

    nc_dataset.attrs['featureType'] = current_feature_type
        
    # Returning dataset, cf_role_coordinate_name and additional information for analysis if asked
    if return_max_simultaneous_measures_variable == True:
        max_simultaneous_measures_variable = list(data_vars)[np.where(max_nb_of_simultaneous_dephs_for_vars == max_simultaneous_measures)[0][0]]
        return nc_dataset, cf_role_coordinate_name, max_simultaneous_measures_variable, max_simultaneous_measures
    else:
        return nc_dataset, cf_role_coordinate_name


# Remove add_offset and scale_factor and convert dtype from int32 variables into float32 (including _FillValue)
def remove_add_offset_and_scale_factor(nc_dataset):
    for var_name in nc_dataset.data_vars:
        if 'add_offset' in nc_dataset.variables[var_name].attrs and nc_dataset[var_name].dtype == 'int32':
            # Get the original variable
            var = nc_dataset[var_name]

            mask = var.values== None
            # Apply the scaling and offset to the _FillValue attribute
            if '_FillValue' in var.attrs:
                float_fill_value = np.float32(9.9692099683868690e+36)
                mask = var.values == var.attrs['_FillValue']
                var.values = np.where(mask, float_fill_value, var.values)

                # var.attrs['_FillValue'] = (var.attrs['_FillValue'] * var.scale_factor + var.add_offset).astype('float32')
                var.attrs['_FillValue'] = float_fill_value
            if 'valid_min' in var.attrs:
                var.attrs['valid_min'] = (var.attrs['valid_min'] * var.scale_factor + var.add_offset).astype('float32')
                var.attrs['valid_max'] = (var.attrs['valid_max'] * var.scale_factor + var.add_offset).astype('float32')
                
            # Apply the scaling and offset to convert the values from int to float
            var.values[~mask] = (var.values[~mask] * var.scale_factor + var.add_offset)
            
            var.values = var.values.astype('float32')
            
            
            # Remove the 'add_offset' and 'scale_factor' attributes
            var.attrs.pop('add_offset', None)
            var.attrs.pop('scale_factor', None)
            
    return nc_dataset


# add and remove some global attributes
    # institution_country
    # add any wanted additionnal attribute with the additionnal_attributes dictionnary
def modify_global_attributes(nc_dataset, 
                             valid_global_attributes_path = '../conf/valid_global_attributes.json', 
                             global_attributes_to_remove_path = '../conf/global_attributes_to_remove.json'):    
    current_dir = Path(__file__)
    valid_global_attributes_path = (current_dir.parent / valid_global_attributes_path).resolve()
    global_attributes_to_remove_path = (current_dir.parent / global_attributes_to_remove_path).resolve()


    
    valid_global_attributes = json.load(open(valid_global_attributes_path,'rb'))
    global_attributes_to_remove = json.load(open(global_attributes_to_remove_path,'rb'))
    
    for attr in global_attributes_to_remove :
        if attr in nc_dataset.attrs:
            # Old attribute needs to be removed and no replacer
            if global_attributes_to_remove[attr]['Replacement Attribute']=='none': 
                nc_dataset.attrs.pop(attr)
            # Old attribute needs to be replaced by new (potentially already existing) one
            else:
                # New attribute already exists : concatenation has to be made
                if global_attributes_to_remove[attr]['Replacement Attribute'] in nc_dataset.attrs: 
                    # if contains url, only keep the url
                    if 'http' in nc_dataset.attrs[attr] :
                        start_indexes = []
                        end_indexes = []
                        start = 0
                        while True:
                            start_index = nc_dataset.attrs[attr].find('http', start)
                            if start_index == -1:
                                break
                            start_indexes.append(start_index)
                            start = start_index + 1
                            end_index = nc_dataset.attrs[attr].find(' ',start)
                            if end_index != -1:
                                end_indexes.append(end_index)
                        urls = []
                        start_indexes, end_indexes = np.array(start_indexes), np.array(end_indexes)
                        for start_index in start_indexes:
                            end_superior_indexes = end_indexes[end_indexes > start_index]
                            if len(end_superior_indexes) > 0:
                                end_index = end_superior_indexes[0]
                            else:
                                end_index = None
                            urls.append(nc_dataset.attrs[attr][start_index:end_index])
                        nc_dataset.attrs.pop(attr)
                        
                        new_replacement_attribute = nc_dataset.attrs[global_attributes_to_remove[attr]['Replacement Attribute']] + ' ' + ' '.join(urls)
                    # if does not contain url, only concatenate
                    else:
                        new_replacement_attribute = nc_dataset.attrs[global_attributes_to_remove[attr]['Replacement Attribute']] + ' ' + nc_dataset.attrs.pop(attr)
                # New attribute does not exist yet
                else:
                    new_replacement_attribute = nc_dataset.attrs.pop(attr)
                    
                # remove extra ' '
                new_replacement_attribute = str(new_replacement_attribute)
                keep_cleaning = True
                while keep_cleaning:
                    if len(new_replacement_attribute) != 0:
                        if new_replacement_attribute[-1] == ' ':
                            new_replacement_attribute = new_replacement_attribute[:-1]
                        else:
                            keep_cleaning = False
                    else:
                        keep_cleaning = False
                nc_dataset.attrs[global_attributes_to_remove[attr]['Replacement Attribute']] = new_replacement_attribute
                
    # New attributes
    for attr in valid_global_attributes:
        # If does not exist, create new attribute
        if attr not in nc_dataset.attrs:
            # If value is empty and attribute is optionnal, then useless writing it
            if not (valid_global_attributes[attr]['Value'] == ' ' and valid_global_attributes[attr]['Mandatory(M)/Optionnal(O)']=='O' ) :
                logger.debug(f'{attr} created. Value is "{valid_global_attributes[attr]["Value"]}"')
                nc_dataset.attrs[attr] = valid_global_attributes[attr]['Value']
        # Attribute already exists in the nc file attributes
        else:
            # If json new attribute value is not empty (==' '), then replace
            if valid_global_attributes[attr]['Value']!=' ':
                logger.debug(f'{attr} value changed from "{nc_dataset.attrs[attr]}" to "{valid_global_attributes[attr]["Value"]}"')
                nc_dataset.attrs[attr] = valid_global_attributes[attr]['Value']
            # Else, do nothing

                
    
    
    # Special case 1 :  institution_country
    if valid_global_attributes['institution_country']['Value'] == ' ' and nc_dataset.attrs['institution_edmo_code']!=' ' :
        sep_list = [';',' ','|',',']
        i=0
        while sep_list[i] not in nc_dataset.attrs['institution_edmo_code'] and i<len(sep_list)-1:
            i+=1
        all_edmo_codes = nc_dataset.attrs['institution_edmo_code'].split(sep_list[i])
        
        if False not in (np.array(all_edmo_codes) == '') and not all_edmo_codes[0].isdigit():
            logger.warning('No institution_edmo_code found: impossible finding institution_country.')
        else:
            # Requesting Seadatanet to find Country
            edmo_countries = []
            for edmo_code in all_edmo_codes:
                # Create an URL object
                # url = 'https://edmo.seadatanet.org/report/'+edmo_code
                url = "https://edmo.seadatanet.org/webservices/edmo/ws_edmo_get_detail/n_code/" + edmo_code
                # Create object page
                page = requests.get(url)  
                
                if page.status_code == 200 and 'xml' in page.headers.get('content-type', ''):
                   # Analyze xml content
                   xml_content = page.content
                   root = ET.fromstring(xml_content)


                   # Recherchez la balise 'c_country' dans l'élément racine
                   c_country = root.find(".//c_country")

                   if c_country is not None:
                       edmo_countries.append(c_country.text)
                   else:
                       logger.warning(f"Page does not contain country name for {edmo_code} EDMO CODE")
                else:
                   logger.warning(f"Error: Failed to retrieve XML from URL for {edmo_code} EDMO CODE")
                
                # soup = BeautifulSoup(page.text, "lxml")
                # dl = soup.find("dl")
                # try :
                #     # dl can be equal to None
                #     dt, dd = dl.find_all('dt'), dl.find_all('dd')
                #     details = {key.text : value.text for key, value in zip(dt,dd)}
                #     edmo_countries.append(details['Country'])
                # except AttributeError:
                #     logger.info("Failed to find information for {edmo_code}")
            logger.debug(f'institution_country value set to "{";".join(edmo_countries)}"')
            nc_dataset.attrs['institution_country'] = ';'.join(edmo_countries)
        
    # Special case 2 : publisher_email
    bigram_publishers_correspondances = {'AR': 'cmems-service@imr.no',
     'BO': 'cmems-service@smhi.se',
     'BS': 'cmems-service@io-bas.bg',
     'GL': 'cmems-service@ifremer.fr',
     'IR': 'cmems-service@puertos.es',
     'MO': 'cmems-service@hcmr.gr',
     'NO': 'cmems-service@bsh.de'}
    nc_dataset.attrs['publisher_email'] = bigram_publishers_correspondances[nc_dataset.attrs['id'][:2]]
    
    return nc_dataset

# Variable attributes are added and changed if needed
def modify_variables_attributes(nc_dataset, attributes_dict_path ='../conf/variables_attributes.json'):
    current_dir = Path(__file__)
    attributes_dict_path = (current_dir.parent / attributes_dict_path).resolve()
    
    
    with open(attributes_dict_path) as json_file :
        attributes_dict = json.load(json_file)
    
    # axis
    coordinates = {'TIME':'T', 'LATITUDE':'Y', 'LONGITUDE':'X'}
    Z_variables_in_file = [x in nc_dataset for x in ['DEPH','HEIGHT','PRES']]
    # DEPH and PRES can be both present. In that case, DEPH is the Z axis
    if len(np.where(Z_variables_in_file)[0]) != 0 :
        coordinates[np.array(['DEPH','HEIGHT','PRES'])[Z_variables_in_file][0]] = 'Z'
    for key, value in coordinates.items():
        nc_dataset[key].attrs['axis'] = value
    
    # adding the variable that handles cf_role as a coordinate
    for var in nc_dataset.variables:
        # deploy_latitude/longitude, precise_latitude/longitude 
        if 'standard_name' in nc_dataset[var].attrs:
            if True in (np.char.find(nc_dataset[var].attrs['standard_name'],['longitude','latitude'])!=-1): 
                coordinates[var] = ''
        # variable that holds the cf role
        if 'cf_role' in nc_dataset[var].attrs:
            coordinates[var] = ''
        
    # _FillValue
    all_fill_values = {'int8' : np.int8(-127),
                        'int32' : np.int32(-2147483647),
                        'float32' : np.float32(9.9692099683868690e+36),
                        'double' : np.double(9.9692099683868690e+36), 
                        'float64' : np.float64(9.9692099683868690e+36)}
    for var in nc_dataset.variables:
        # remove older coordinates attribute if it exists. This attribute will be properly re-written in the next instructions
        if 'coordinates' in nc_dataset.variables[var].attrs:
            nc_dataset.variables[var].attrs.pop('coordinates')
        
        # avoiding the S1 dtype (string bytes) : they are managed at the end of the file.
        if var not in coordinates.keys() and str(nc_dataset[var].dtype) in all_fill_values.keys():
            nc_dataset[var].attrs['_FillValue'] = all_fill_values[str(nc_dataset[var].dtype)]
        
    # ancillary_variables
    for var in nc_dataset.variables:
        ancillary_variables = np.array(nc_dataset.variables)[np.char.find(list(nc_dataset.variables),var + '_')!=-1]
        if 'ADJUSTED' not in var:
            ancillary_variables = ancillary_variables[np.char.find(ancillary_variables,'ADJUSTED')==-1]
        if len(ancillary_variables) > 0:
            nc_dataset.variables[var].attrs['ancillary_variables'] = ' '.join(ancillary_variables)
             
    # remove variable attributes
    variable_attributes_to_remove = ['QC_indicator','conventions']
    for var in nc_dataset.variables:
        for attr in variable_attributes_to_remove:
            if attr in nc_dataset[var].attrs:
                nc_dataset[var].attrs.pop(attr)
                
    # flag_meanings and flag_values
    flag_meanings_and_values = {'_QC': {'flag_meanings':'no_qc_performed good_data probably_good_data bad_data_that_are_potentially_correctable bad_data value_changed value_below_detection nominal_value interpolated_value missing_value',
                                        'flag_values': np.array([0,1,2,3,4,5,6,7,8,9],dtype='int8')},
                                '_DM': {'flag_meanings':'real-time adjusted-in-real-time delayed-mode',
                                        'flag_values':'R, A, D'},
                                'DIRECTION': {'flag_meanings':'ascending_profile descending_profile unknown',
                                              'flag_values':'A, D, U'}
                                }
    for var in nc_dataset.variables:
        set_attributes = False
        for key in flag_meanings_and_values:   # DIRECTION can be a variable                 
            if key in var :
                set_attributes = True
                kept_key = key
        if set_attributes:
            nc_dataset.variables[var].attrs['flag_values'] = flag_meanings_and_values[kept_key]['flag_values']
            nc_dataset.variables[var].attrs['flag_meanings'] = flag_meanings_and_values[kept_key]['flag_meanings']
                    
    # Adding user prompted dict attributes
    for var in attributes_dict:
        for attr in attributes_dict[var]:
            nc_dataset[var][attr] = attributes_dict[var][attr]
            
    # encoding settings
    no_coordinate_attribute_variables_names = ['TIME_QC','POSITION_QC']
    for var in nc_dataset.variables:
        if 'axis' in nc_dataset[var].attrs:
            if nc_dataset[var].attrs['axis'] == 'Z':
                no_coordinate_attribute_variables_names.append(var+'_QC')
    for var in nc_dataset.variables:
        # only set the "coordinates" attribute for the variables that are not coordinates themselves
        if var not in coordinates.keys() and var not in no_coordinate_attribute_variables_names and var != 'DEPLOYMENT' :
            nc_dataset[var].encoding['coordinates'] = " ".join(list(coordinates.keys())) # essential for correctly encoding the "coordinates" attribute
        else:
            nc_dataset[var].encoding['coordinates'] = None
                
    return nc_dataset, list(coordinates.keys())


# set LATITUDE and LONGITUDE as scalar variables in all cases and remove POSITION_QC as their ancillary variable
    # if several LATITUDE and LONGITUDE values, chose the latest valid position (POSITION_QC in [1 or 7])
# category_of_position can be set to : "gps", "deployments", "nominal"
    # if 'nominal' : remove POSITION_QC
    # if 'deployments' : remove POSITION_QC + creating , DEPLOY_LONGITUDE, DEPLOY_LATITUDE, DEPLOYMENT variables
    # if 'gps' : creating PRECISE_LONGITUDE, PRECISE_LATITUDE variables + setting POSITION_QC as their ancillary variable
def timeseries_position_coordinates_management(nc_dataset):
    coordinate_variables = {'LATITUDE': nc_dataset['LATITUDE'], 'LONGITUDE': nc_dataset['LONGITUDE'] }
    POSITION_QC = nc_dataset['POSITION_QC']
    
    # finding distinct positions. If more than 5% of the total amount of positions are different, then the buoy is considered as holding a GPS sensor. Else, only considered as having nominal position(s)
    distinct_positions, indexes = np.unique(np.column_stack((nc_dataset['LONGITUDE'], nc_dataset['LATITUDE'])),axis = 0, return_index = True)
    distinct_positions_qcs = POSITION_QC[indexes]
    
    where_good_qc = np.where(np.isin(distinct_positions_qcs,[1,7]))[0]
    if len(where_good_qc) == 0:
        logger.warning('No good POSITION_QC values found. 1 and 7 values are desired. Please update POSITION_QC values. 0 values chosen by default')
        where_good_qc = np.where(distinct_positions_qcs)
    valid_distinct_positions, valid_indexes = distinct_positions[where_good_qc], indexes[where_good_qc]
    
    ratio = len(valid_distinct_positions)/len(nc_dataset['POSITION_QC'][np.isin(nc_dataset['POSITION_QC'],[1,7])])
    if ratio > 0.01 or len(valid_distinct_positions) > 200:
        category_of_positions = 'gps'
    elif len(valid_distinct_positions) > 1 :
        category_of_positions = 'several_nominal_positions'
    else:
        category_of_positions = 'one_nominal_position'
    
    new_nc_dataset = nc_dataset.copy()
    
    # valid indexes
    if len(valid_indexes)==0:
        logger.warning('nominal valid position only (_QC==7)')
        valid_indexes = np.where(nc_dataset['POSITION_QC'].values==7)[0]
    last_valid_index = valid_indexes[-1]
    
    # in all cases, LONGITUDE and LATITUDE lose POSITION_QC as an ancillary variable
    for key in coordinate_variables.keys() :
        new_nc_dataset = new_nc_dataset.drop_dims(key)
        new_nc_dataset[key] = nc_dataset[key].values[last_valid_index]
        new_nc_dataset[key].attrs =  nc_dataset[key].attrs
        if 'ancillary_variables' in new_nc_dataset[key].attrs:
            del new_nc_dataset[key].attrs['ancillary_variables']
    
    new_coords = []
    float_fill_value = np.float32(9.9692099683868690e+36)
    if category_of_positions.lower() == 'gps':
        # create precise coordinate variables
        # POSITION_QC is set as an ancillary variable since it is copied from the original coordinate variable attributes
        for key in coordinate_variables.keys():
            new_key = 'PRECISE_' + key
            new_nc_dataset[new_key] = xr.DataArray(data = coordinate_variables[key].values,
                                                   dims = ('TIME'),
                                                   attrs = coordinate_variables[key].attrs
                                                   )
            new_nc_dataset[new_key].attrs['long_name'] = new_nc_dataset[new_key].attrs['long_name']
            new_nc_dataset[new_key].attrs['_FillValue'] = float_fill_value
            # remove the copy of "axis" attribute
            del new_nc_dataset[new_key].attrs['axis'] # axis is only an attribute kept for nominal positions + TIME + DEPH
            new_coords.append(new_key)   
            
    elif category_of_positions.lower() == 'several_nominal_positions':
        # creating 'DEPLOYMENT' variable
        new_nc_dataset['DEPLOYMENT'] = valid_indexes
        new_nc_dataset.variables['DEPLOYMENT'].attrs['long_name'] = 'index of the first time after (re)deployment'
        new_nc_dataset.variables['DEPLOYMENT'].attrs['compress'] = 'TIME'
        
        # re-write the variable 
        dims = ['DEPLOYMENT']
        for key in coordinate_variables.keys():
            new_key = 'DEPLOY_' + key
            var = {new_key : xr.Variable(dims, coordinate_variables[key][:][valid_indexes].astype(
                                                                 coordinate_variables[key].values.dtype
                                                                 )
                                                             )
                }
            new_nc_dataset = new_nc_dataset.assign(var)
            # give correct attributes
            new_nc_dataset[new_key].attrs = coordinate_variables[key].attrs
            new_nc_dataset[new_key].attrs['long_name'] = coordinate_variables[key].attrs['standard_name'][0].upper() + coordinate_variables[key].attrs['standard_name'][1:].lower() + ' of each deployment'
            new_nc_dataset[new_key].attrs['standard_name'] = 'deployment_' + coordinate_variables[key].attrs['standard_name']
            # remove the copy of "axis" attribute
            del new_nc_dataset[new_key].attrs['axis'] # axis is only an attribute kept for nominal positions + TIME + DEPH
            # remove the copy of "ancillary_variables" attribute
            if 'ancillary_variables' in new_nc_dataset[new_key].attrs: # happened on BS files
                del new_nc_dataset[new_key].attrs['ancillary_variables']
            new_coords.append(new_key)
    else:
        for key in coordinate_variables.keys():
            new_nc_dataset[key] = coordinate_variables[key].values[last_valid_index]
            new_nc_dataset[key].attrs = coordinate_variables[key].attrs
            if 'ancillary_variables' in new_nc_dataset[key].attrs: # happened on BS files
                # remove the copy of "ancillary_variables" attribute
                del new_nc_dataset[key].attrs['ancillary_variables']

    # to_add_starting_array = new_coords.copy()
    to_add_starting_array = []
    if category_of_positions.lower()!='gps': # POSITION_QC only useful when precise positions exist
        del new_nc_dataset['POSITION_QC']
        
        if category_of_positions.lower()=='several_nominal_positions':
            to_add_starting_array.append('DEPLOYMENT')
    else :
        new_nc_dataset['POSITION_QC'] = new_nc_dataset['POSITION_QC'].rename({'POSITION':'TIME'})
        
    
            
    return new_nc_dataset, new_coords, to_add_starting_array
        
        

# rename the cf_role variable main dimension from 'STRING*' to 'STRLEN'
# remove "_Encoding" (automatically written by xarray) attribute
def cf_role_variable_management(nc_path, format, cf_role_coordinate_name):
    nc =  netCDF4.Dataset(nc_path, 'r+', format=format)
    # rename the cf_role variable main dimension from 'STRING*' to 'STRLEN'
    if format == "NETCDF4_CLASSIC":
        # rename the char string attribute of the new created TRAJECTORY variable
        # https://stackoverflow.com/questions/55479484/add-a-new-dimension-to-a-netcdf-file-created-by-xarray/55485458#55485458
        nc.renameDimension(nc.variables[cf_role_coordinate_name].dimensions[0], 'STRLEN')
    
    if '_Encoding' in nc.variables[cf_role_coordinate_name].__dict__:
        nc.variables[cf_role_coordinate_name].delncattr('_Encoding')
    nc.close()
    
    
# These variables cause xarray error when exporting. Best solution is to use netCDF4 to fix the problem.
def variables_with_bytestr_fill_values_managing(nc_path, format, variables_with_byte_FillValues, original_nc_file_xr_dataset):    
    # add the b' ' _FillValue attribute to DC_REFERENCE (and others than can exist)
    nc =  netCDF4.Dataset(nc_path, 'r+', format=format)
    for var in variables_with_byte_FillValues:    
        # Create a new variable with the same data and attributes, including fill_value
        old_variable = original_nc_file_xr_dataset.variables[var]
        
        # Creating the possibly missing dimensions
        for dim in old_variable.dims:
            if dim not in nc.dimensions:
                nc.createDimension(dim, original_nc_file_xr_dataset.dims[dim])
        
        # Regenerating the variable
        new_variable = nc.createVariable(var, old_variable.dtype, old_variable.dims, fill_value=b' ')
        
        # Copy the data from the old variable to the new variable
        new_variable[:] = old_variable[:]
        
        # Copy the attributes from the old variable to the new variable
        dict_without_fill_value = old_variable.attrs.copy()
        dict_without_fill_value.pop('_FillValue')
        dict_without_fill_value.pop('conventions')
        new_variable.setncatts(dict_without_fill_value)

    # Save the changes and close the file
    nc.close()


# Compressing the nc file
def compression_management(input_path, output_path, complevel, format):
    # Open the original NetCDF file
    original_file = netCDF4.Dataset(input_path, 'r', format=format)

    # Create a new NetCDF file with compression
    compressed_file = netCDF4.Dataset(output_path, 'w', format=format)

    # Copy dimensions from the original file to the compressed file
    for dim_name, dim in original_file.dimensions.items():
        compressed_file.createDimension(dim_name, len(dim))

    # Copy variables and their attributes from the original file to the compressed file
    for var_name, var in original_file.variables.items():
        attrs_dict = var.__dict__
        if '_FillValue' in var.ncattrs():
            # Create the variable in the compressed file with compression enabled
            compressed_var = compressed_file.createVariable(var_name, var.dtype, var.dimensions, fill_value=var.getncattr('_FillValue'), zlib=True, complevel=9)
            attrs_dict.pop('_FillValue')
        else:
            # Create the variable in the compressed file with compression enabled
            compressed_var = compressed_file.createVariable(var_name, var.dtype, var.dimensions, zlib=True, complevel=complevel)

        # Copy the variable's data
        compressed_var[:] = var[:]
        
        # Copy the variable's attributes
        compressed_var.setncatts(attrs_dict)

    
    # Copy global attributes from the source file to the destination file
    for attr_name in original_file.ncattrs():
        compressed_file.setncattr(attr_name, original_file.getncattr(attr_name))

    
    # Close the files
    original_file.close()
    compressed_file.close()
    
    os.remove(input_path)
