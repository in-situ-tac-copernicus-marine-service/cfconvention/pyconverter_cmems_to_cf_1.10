# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 13:50:31 2023

@author: gmureau
"""


#%% Dependencies
import os
os.environ['HDF5_USE_FILE_LOCKING'] = 'FALSE' # important to execute this command before calling xarray or netcdf4 library. Or else, opening a dataset with windows will generate an HDF error
import numpy as np
import pandas as pd

#%% Managing paths

# chose DB or MO (or else)
type_of_platforms = 'DB'
platforms_path = '/home/ref-copernicus-insitu/INSITU_GLO_PHYBGCWAV_DISCRETE_MYNRT_013_030/cmems_obs-ins_glo_phybgcwav_mynrt_na_irr/history/' + type_of_platforms


wd_path = '/home1/datawork/gmureau/coriolis/issues/feature_type_profiles_or_not/'
all_files = os.listdir(platforms_path)
all_files = np.array(all_files)

report_path = os.path.join(wd_path,'reports')
try:
    os.mkdir(report_path)
except FileExistsError:
    pass


# Keeping all the GL files (no wave spectras)
selected_platform_files = os.listdir(platforms_path)
selected_platform_files = np.array(selected_platform_files)
selected_platform_files = selected_platform_files[np.char.find(selected_platform_files,'.nc')!=-1]
selected_platform_files = selected_platform_files[np.char.find(selected_platform_files,'WS')==-1]
selected_platform_files = selected_platform_files[np.char.find(selected_platform_files,'GL')!=-1]



#%% feature_type_analysis_function
import multiprocess as mp

# initial arguments
return_max_simultaneous_measures_variable=True
if type_of_platforms == 'DB':
    feature_type ='trajectory'
elif type_of_platforms == 'MO' :
    feature_type = 'timeSeries'
else:
    feature_type = ''
    

# Setting the featuretype
# Also creates the coordinate variable that handles the cf_role
# Result largely depends on the depths repartition
# If there are 4 or more different depths at the same time, for the same parameter, then the featuretype become "timeseriesProfile" or "trajectoryProfile"
def changing_feature_type(nc_datasets_paths, feature_type, return_max_simultaneous_measures_variable=False):
    import os
    os.environ['HDF5_USE_FILE_LOCKING'] = 'FALSE' # important to execute this command before calling xarray or netcdf4 library. Or else, opening a dataset with windows will generate an HDF error
    import numpy as np
    import pandas as pd
    import xarray as xr
    
    if return_max_simultaneous_measures_variable == True:
        columns = ['file_name','feature_type','cf_role_coordinate_name','max_simultaneous_measures_nb','max_simultaneous_measures_variable']
    else:
        columns = ['file_name','feature_type','cf_role_coordinate_name']
    files_ft_information = pd.DataFrame(columns = columns)
    
    
    for nc_dataset_path in nc_datasets_paths:
    
        nc_dataset = xr.open_dataset(nc_dataset_path, engine= 'netcdf4')
        # Data variables
        data_vars = nc_dataset.data_vars
        
        # We will keep only the variable with the biggest number of levels for each file
        # Maximum amount of simultaneous (for a given time) measures for each parameter (data variable)
        max_nb_of_simultaneous_dephs_for_vars = []
        for var in data_vars :
            if nc_dataset[var].dtype in ['float64', 'float32', 'int32', 'int64'] and var not in ['DEPH','PRES'] and 'QC' not in var:
                if '_FillValue' in nc_dataset[var].attrs:
                    old_fill_value = nc_dataset[var].attrs['_FillValue']
                else:
                    old_fill_value = np.nan
                new_fill_value = -9999
                # Converting to int16 : prevents from using too much memory. The exact measure value is not important. What we only are looking for is presence of absence of measure and speed of computation
                if np.isnan(old_fill_value):
                    nc_dataset[var] = nc_dataset[var].fillna(new_fill_value)   
                else:
                    nc_dataset[var] = nc_dataset[var].where(nc_dataset[var] != old_fill_value, new_fill_value)
                nc_dataset[var] = nc_dataset[var].astype('int16')  
                
                # Finding all indexes where measures do exist
                not_fill_value = ~(nc_dataset[var].values == new_fill_value)
                # Getting time indexes where measures do exist
                where_not_fv = np.where(not_fill_value)
                
                # We will count the maximum amount of depths where there is data, for each time index
                unique, counts = np.unique(where_not_fv[0], return_counts=True)
                max_nb_of_simultaneous_dephs_for_vars.append(np.nanmax(counts))
            else:
                max_nb_of_simultaneous_dephs_for_vars.append(np.nan)
        
        # Find the data variable (parameter) index for which the number of simultaneous (same given time) values is maximum
        max_simultaneous_measures_nb = np.nanmax(max_nb_of_simultaneous_dephs_for_vars)
        
        # Creating the coordinate variable
        if 'timeseries' in feature_type.lower() : #timeSeries, timeSeriesProfile
            feature_type = 'timeSeries'
            cf_role_coordinate_name = 'STATION'
        else: # trajectory, trajectoryProfile, Profile
            if 'profile' in feature_type.lower():
                feature_type = feature_type.lower()
                feature_type = feature_type[:feature_type.index('profile')]
            cf_role_coordinate_name = feature_type.upper()
            
        # Setting the featureType
            # profile
        if max_simultaneous_measures_nb > 4 and 'profile' not in feature_type:
            feature_type = feature_type + 'Profile'
              
        # Returning dataset, cf_role_coordinate_name and additional information for analysis if asked
        if return_max_simultaneous_measures_variable == True:
            try:
                max_simultaneous_measures_variable = list(data_vars)[np.where(max_nb_of_simultaneous_dephs_for_vars == max_simultaneous_measures_nb)[0][0]]
                files_ft_information.loc[len(files_ft_information)] = [ nc_dataset.attrs['id'], feature_type, cf_role_coordinate_name, int(max_simultaneous_measures_nb), max_simultaneous_measures_variable  ]
            except IndexError: # in case of the file only has coordinates variables (often when DB)
                files_ft_information.loc[len(files_ft_information)] = [ nc_dataset.attrs['id'], feature_type, cf_role_coordinate_name, 'None', 'None'  ]
        else:
            files_ft_information.loc[len(files_ft_information)] = [ nc_dataset.attrs['id'], feature_type, cf_role_coordinate_name, max_simultaneous_measures_nb, max_simultaneous_measures_variable  ]
    
    return files_ft_information



nc_datasets_paths = np.char.add(platforms_path+'/',selected_platform_files)
now = np.datetime64('now')

if __name__ == '__main__': # avoids running parallel if the file is called as a method for another script file

    num_processes = mp.cpu_count()  # get the number of available CPUs
    number_of_files_to_process = len(nc_datasets_paths) # default ; for processing less files (debug)
    # number_of_files_to_process = 300 # default ; for processing less files (debug)
    offset = 0 # default ; for testing files starting from another index
    # offset = 800
    chunk_size = number_of_files_to_process // num_processes  # divide the data into chunks

    
    with mp.Pool(processes=num_processes) as pool:
        
        # list of argument tuples. Number of tuples = number of CPU cores
        args = []
        for i in range(0, len(nc_datasets_paths[:chunk_size*num_processes]), chunk_size):
            index = i + offset
            # for each set of files, give arguments
            function_tuple = [nc_datasets_paths[index:index+chunk_size],
                              feature_type,
                              return_max_simultaneous_measures_variable]
            args.append(function_tuple)

        # remaining_files = nc_datasets_paths[-(number_of_files_to_process %  num_processes):]
        remaining_files = nc_datasets_paths[offset+chunk_size*num_processes:offset+number_of_files_to_process]
        args[-1][0] = np.concatenate([args[-1][0],remaining_files])
        
        results = pool.starmap(changing_feature_type, args)
    
    files_ft_information = pd.DataFrame(columns = results[0].columns)
    for i in range(len(results)):
        files_ft_information = pd.concat([files_ft_information, results[i]], ignore_index=True)

processing_time = str(np.datetime64('now')-now)
print('Processing time: ', processing_time)


files_ft_information.to_csv(report_path+'/'+ type_of_platforms + '_feature_types.csv', index = False)
