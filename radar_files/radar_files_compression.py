# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 16:33:17 2023

@author: gmureau
"""

#%% Dependancies
import xarray as xr
import numpy as np
import netCDF4
import collections.abc
# dict of dicts update
def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d

import sys



#%% 1) Opening nc file

wd = r'./'

# trajectory files
rad_file = "GL_TV_HF_HFR-Finnmark-Total.nc"
# rad_file = "GL_TV_HF_HFR-US-EastGulfCoast-Total_202212.nc"
# rad_file = "GL_TV_HF_HFR-US-WestCoast-Total_2021.nc" # biggest file

nc_path = wd + '\\' + rad_file

# decode_cf = False prevents xarray of interprating the variables and change their DataTypes
nc_file = xr.open_dataset(nc_path, engine='netcdf4', decode_cf=False)


is_compression = "not_compressed"
is_compression = "compressed"

# for new nc file output path
particle = 'rad_' + is_compression

#%% 2) Modifying file
new_nc_file = nc_file.copy()

# update "Conventions" attribute
new_nc_file.attrs['Conventions'] = 'CF-1.8' + nc_file.attrs['Conventions'][np.char.find(nc_file.attrs['Conventions'],' '):]




# cannot have a string byte _FillValue
# replacing np.bytes_ with strings (temporary solution)
variables_with_byte_FillValues = []
for var in nc_file.variables:
    if '_FillValue' in nc_file[var].attrs:
        if type(nc_file[var].attrs["_FillValue"]) == np.bytes_:
            variables_with_byte_FillValues.append(var)
            
for var in variables_with_byte_FillValues:
    new_nc_file.variables[var].attrs['_FillValue']=""
    new_nc_file[var].encoding['coordinates'] = None



# compressing data
if is_compression!= 'compressed' :
    comp = {'zlib':None, 'complevel':0}
else:
    comp = {'zlib':True, 'complevel':9}
# encoding_comp = {var: comp for var in new_nc_file.data_vars} # data variables only
encoding_comp = {var: comp for var in new_nc_file.variables} # all variables including coordinates
# avoid not wanted '_FillValue' attribute for variables that did not have it before
encoding_fill_values={i: {'_FillValue': None}
          for i in variables_with_byte_FillValues
          }
# compression zlib 9 + avoid new '_FillValue' attrs
encoding = update(encoding_comp, encoding_fill_values)



    

# export following the encoding rules, in NCDF4 classic format
new_nc_file.to_netcdf(wd+'\\' + rad_file[:-3] + '_changed_' + particle + '.nc',
                      encoding=encoding,
                      format = 'NETCDF4_CLASSIC',
                      )



# rename the char string attribute of the new created TRAJECTORY variable
# https://stackoverflow.com/questions/55479484/add-a-new-dimension-to-a-netcdf-file-created-by-xarray/55485458#55485458
# nc =  netCDF4.Dataset(wd+'\\' + rad_file[:-3] + '_changed_' + particle + '.nc', 'r+', format='NETCDF4_CLASSIC')
# nc.renameDimension(nc.variables[dimension_to_rename].dimensions[0], 'NAME_STRLEN')
# nc.close()


