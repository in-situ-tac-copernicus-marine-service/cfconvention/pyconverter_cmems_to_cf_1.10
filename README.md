Here are the cf convention 1.11 conversion scripts files and some input and output examples (nc files)
https://gitlab.ifremer.fr/in-situ-tac-copernicus-marine-service/cfconvention/pyconverter_cmems_to_cf_1.10

The changes are described here : 
https://docs.google.com/document/d/1Edm3f-C19Zp5uJww8YAR3zWtaX5nxwsppJ2yIIQMfpA/edit?usp=sharing
https://docs.google.com/spreadsheets/d/1iMS3g1V9lvtMu2qSUGwRL2GaXIQ5WnZFJzaU2BxGKBE/edit?usp=sharing
https://docs.google.com/spreadsheets/d/1GI9iu1Wp3hidboXdRpIXDbFPTRSS8-YIhfZCRh90MGE/edit?usp=sharing

Dependancies management : scripts should run easily since the used dependencies are common, but if you are encountering dependencies issues, please run this command:
pip install -r ./requirements.txt

Files are sorted by featuretypes:

- timeSeries and timeSeriesProfile
- trajectory and trajectoryProfile
- profiles

The "Conventions" attribute of each nc file can be set on "CF-1.8", in order to pass the cf-checker test : 
https://github.com/cedadev/cf-checker
By default, it is set on "CF-1.11" in order to satisfy the copernicus format checker

You can now just run the script "main.py" followed by arguments in order to convert your data from old cf convention to 1.11. Here is the list of the arguments:
- `--input` : path to nc file or directory that contains nc files (if directory set, all nc files inside will be converted)
- `--output`: optionnal argument. path to output nc file (if input is only a single nc file) or output directory. If a directory is set, all output nc files will have the same name than input nc files. If --output not set, output files will be in the same directory as input files, with a suffix in their names.
 - `--type_of_data` : optionnal argument. Used to force featuretype and used conversion script. Set it to "trajectory" or "timeseries". If not set, script will use the cdm_data_type attribute to determine which conversion script to use
 - `--logging_level` : optionnal arguement. Used to have a report of the process. Can be set to "info" or "debug". If not given, automatically set to "info".



Examples:
`python main.py --input=/path/to/my/dir/ --output=/path/to/other/dir`
--> all the nc files in `/path/to/my/dir` will be converted and created inside of the `/path/to/other/dir`, with the same name. Featuretypes will be automatically detected
`python main.py --input=/path/to/my/dir/file.nc`
--> featuretype of "file.nc" will be automatically detected from the "cdm_data_type" global attribute, and a converted file name "file+{suffix}.nc" will be created inside of the "/path/to/my/dir" directory
`python main.py --input = path/to/my/dir --type_of_data=trajectory`
--> script will try to apply trajectory/trajectoryprofile converter to the files inside of the "/path/to/my/dir" directory. Maybe will it fail (fails are referenced inside of the console)



If you need to add or remove global or variable attributes, please refer to the "./conf" directory and "./conf/README.md" file