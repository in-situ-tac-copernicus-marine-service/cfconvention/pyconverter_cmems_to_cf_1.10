# -*- coding: utf-8 -*-
"""
Created on Fri Jun 30 17:13:19 2023

@author: gmureau
"""

#%% 

# output_nc_file = xr.open_dataset(wd+'\\' + buoy_file[:-3] + '_changed_' + particle + '.nc', engine='netcdf4', decode_cf=False)

# # adding add_offset and scale_factor attributes + converting values (including _FillValue) to 'float32' dtype
# for var_name in nc_file.data_vars:
#     if 'add_offset' in nc_file.variables[var_name].attrs:
#         # Get the original variable
#         var = output_nc_file[var_name]
        
#         # Apply the scaling and offset to convert the values from int to float
#         var.values = (var.values / nc_file.variables[var_name].attrs['scale_factor'] - nc_file.variables[var_name].attrs['add_offset']).astype('int32')
        
#         # Apply the scaling and offset to the _FillValue attribute
#         if '_FillValue' in nc_file[var_name].attrs:
#             var.attrs['_FillValue'] = (var.attrs['_FillValue'] / nc_file.variables[var_name].attrs['scale_factor'] - nc_file.variables[var_name].attrs['add_offset']).astype('int32')
        
#         # Optional: add the 'add_offset' and 'scale_factor' attributes
#         var.attrs['add_offset'] = nc_file.variables[var_name].attrs['add_offset']
#         var.attrs['scale_factor'] = nc_file.variables[var_name].attrs['scale_factor']
        
        
        
        
        
# # sets the order of the variables in the nc file
# starting_array = np.array(['TIME', 'TIME_QC','LONGITUDE', 'LATITUDE', 'POSITION_QC', 'DEPH', 'DEPH_QC', 'DEPLOYMENT',
#                             'DEPLOY_LONGITUDE', 'DEPLOY_LATITUDE', 'DEPLOY_POSITION_QC'])
# # sets the coordinates in the nc file
# coordinates = np.array(['TIME','LONGITUDE','LATITUDE', 'DEPH', 'DEPLOY_LONGITUDE', 'DEPLOY_LATITUDE','STATION_NAME'])


# # compressing data
# if is_compression!= 'compressed' :
#     comp = {'zlib':None, 'complevel':0}
# else:
#     comp = {'zlib':True, 'complevel':9}
# # encoding_comp = {var: comp for var in new_nc_file.data_vars} # data variables only
# encoding_comp = {var: comp for var in output_nc_file.variables} # all variables including coordinates
# # avoid not wanted '_FillValue' attribute for variables that did not have it before
# encoding_fill_values={i: {'_FillValue': None}
#           for i in np.delete(starting_array,starting_array=="TIME_QC")
#           }
# # compression zlib 9 + avoid new '_FillValue' attrs
# encoding = update(encoding_comp, encoding_fill_values)

# # temporary solution (cannot have a string byte _FillValue)
# output_nc_file.variables['DC_REFERENCE'].attrs['_FillValue']=""
# # new_nc_file['DC_REFERENCE'].encoding['_FillValue'] = nc_file['DC_REFERENCE'].attrs['_FillValue']




        
# output_nc_file.to_netcdf(wd+'\\' + buoy_file[:-3] + '_changed_' + particle + '_recovered.nc',
#                       encoding=encoding,
#                       format = 'NETCDF4_CLASSIC',
#                       )
