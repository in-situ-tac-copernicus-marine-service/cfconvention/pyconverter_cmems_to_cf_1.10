# -*- coding: utf-8 -*-
"""
Created on Fri Oct 13 11:11:08 2023

@author: gmureau
"""
import argparse
import sys
import os 
import xarray as xr
import numpy as np
# Get the absolute path of the running script directory
script_directory = os.path.dirname(os.path.abspath(__file__))
from functions.timeSeries_and_timeSeriesProfile.generating_cf1_11_timeseries_and_timeseriesProfile_files import timeseries_and_timeseriesprofiles_conversion
from functions.trajectory_and_trajectoryProfile.generating_cf1_11_trajectory_and_trajectoryProfile_files import trajectory_and_trajectoryprofiles_conversion
import logging


logging_levels = {10:"DEBUG", 20:"INFO"}

def main():

    # Create an ArgumentParser object
    parser = argparse.ArgumentParser(description="This program is used to convert old cf files to the cf1.11 format.")

    # Add arguments using the add_argument method
    parser.add_argument("--input", help="Input file or folder")
    parser.add_argument("--output", help='Output file or folder. If not given, file will be saved with a "_converted" suffix in the input folder')
    parser.add_argument("--type_of_data", help='"timeseries", "trajectory" or "auto-detect". If not given, automatically set to "auto-detect"')
    parser.add_argument("--logging_level", help='"info" or "debug". If not given or wrong, automatically will be set to "info".')
    parser.add_argument("--filter", help='Files that contain the value will be removed. If not given or wrong, automatically will be set to "".')
    parser.add_argument("--ignore_errors", help='If set to "True", when "input" is a folder, the files for which conversion is in error will be reported and the script will move on the next file without stopping. Set to "False" by default')

    # Analyze the arguments from the command line
    args = parser.parse_args()
    
    # managing the arguments
    input = str(args.input)
    output = str(args.output)
    type_of_data = str(args.type_of_data)
    logging_level = str(args.logging_level)
    filter = str(args.filter)
    ignore_errors = args.ignore_errors
    
    # Create a logger
    logger = logging.getLogger(__name__)
    # Create handler with personalized format
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s'))
    # Chose the logging level
    if logging_level.lower() == 'debug':
        # Configure only one StreamHandler to display logs in the console
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    # Add the handler to the specific logger
    logger.addHandler(handler)
    
    if str(ignore_errors).lower() != "true":
        ignore_errors = False
    else:
        ignore_errors = True
    

    
    logger.info(f'logging_level : {logging_levels[logger.getEffectiveLevel()]}')
    logger.info('Filled arguments:')
    logger.info(f'input:{input}')
    logger.info(f'output:{output}')
    logger.info(f'type_of_data:{type_of_data}\n')
    if not os.path.exists(input):
        logger.critical('Missing correct input file. Please use "--input" argument with a valid value.')
        sys.exit()
    else:
        input = os.path.abspath(input)
     
    if output.lower()!='none':
        if not os.path.isdir(os.path.dirname(str(output))):
            logger.warning('Incorrect output arg. Set to "none"')
            output = 'none'
        else:
            output = os.path.abspath(output)
            if os.path.isdir(output)==False and os.path.isdir(input)==True :
                logger.critical('Output is a single file whereas input is a folder. Please try again.')
                sys.exit('')
    else :
        output = output.lower()
    if type_of_data.lower() not in ['profile','trajectory','timeseries']:
        type_of_data = "auto-detect"
        logger.info('type_of_data set to "auto-detect"\n')
        
    # take the directory address and make a list of input_files
    if os.path.isdir(input):
        input_files = os.listdir(input)
        input_files = np.array(input_files)[np.char.find(input_files,'.nc')!=-1]
        dir1 = input
    else:
        input_files = [os.path.basename(input)]
        dir1 = os.path.dirname(input)
    input_files = np.array(input_files)[~(np.char.find(input_files,filter)!=-1)]
    input_files = [os.path.join(dir1,file) for file in input_files]
    logger.debug('list of input files:\n' + str(input_files))
    
    # same thing for output
    if os.path.isdir(output):
        dir2 = output
        output_files = [file.replace(dir1,dir2) for file in input_files]
    else:
        output_files = ["none" for file in input_files]
    logger.debug('list of output files:\n' + str(output_files))

    
    for i in range(len(input_files)) :
        success = True
        input_file = input_files[i]
        output_file = output_files[i]
        logger.info('Input file : ' + os.path.basename(input_file))

        if type_of_data in ['timeseries','trajectory']:
            file_data_type = type_of_data
        elif type_of_data == 'auto-detect':
            dataset = xr.open_dataset(input_file, engine='netcdf4', decode_cf=False)
            if 'cdm_data_type' in dataset.attrs:
                file_data_type = dataset.attrs['cdm_data_type'].lower()
            elif 'data_type' in dataset.attrs:
                distinct_data_types = np.array(['timeseries','time-series','trajectory','profile'])
                data_type_indices = np.char.find(dataset.attrs['data_type'].lower(), distinct_data_types)
                selected_data_types = distinct_data_types[np.where(data_type_indices !=-1)]
                file_data_type = selected_data_types[0].replace('-','')
            else:
                logger.error("auto-detect has failed. No valid data type.")
            logger.info(f'auto-detected as {file_data_type}')
        else:
            logger.critical(f'Incorrect type_of_data :"{type_of_data}". Please try again.')
            sys.exit()

        
        if file_data_type == 'timeseries':
            if ignore_errors :
                try:
                    output_path = timeseries_and_timeseriesprofiles_conversion(input_file, output_file, file_data_type)
                except Exception as e:
                    logger.error(f"Error detected : {e}")
                    success = False
                    pass
            else:
                output_path = timeseries_and_timeseriesprofiles_conversion(input_file, output_file, file_data_type)
        elif file_data_type in ['trajectory','profile']:
            if ignore_errors:
                try:
                    output_path = trajectory_and_trajectoryprofiles_conversion(input_file, output_file, file_data_type)
                except Exception as e:
                    logger.error(f"Error detected : {e}")
                    success = False
                    pass
            else:
                output_path = trajectory_and_trajectoryprofiles_conversion(input_file, output_file, file_data_type)

        else :
            success = False
            logger.error("auto-detect chosen + file_data_type not found or not valid")
            

        if success:
            logger.info('Output file : ' + str(output_path))
            logger.info("Status : ok\n")
        else:
            logger.error("Status : ko\n")

        
        
if __name__ == "__main__":
    main()